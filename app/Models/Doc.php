<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Doc
 * 
 * @property int $id
 * @property string $name
 * @property Carbon $create
 * @property Carbon $update
 * 
 * @property Collection|DocsTpv[] $docs_tpvs
 *
 * @package App\Models
 */
class Doc extends Model
{
	protected $table = 'docs';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int'
	];

	protected $dates = [
		'create',
		'update'
	];

	protected $fillable = [
		'name',
		'create',
		'update'
	];

	public function docs_tpvs()
	{
		return $this->hasMany(DocsTpv::class, 'tipo_doc');
	}
}
