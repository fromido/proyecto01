<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DocsParam
 * 
 * @property int $id
 * @property string $value
 * @property string $table_reference
 * 
 * @property Collection|DocsInt[] $docs_ints
 * @property Collection|DocsString[] $docs_strings
 *
 * @package App\Models
 */
class DocsParam extends Model
{
	protected $table = 'docs_params';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int'
	];

	protected $fillable = [
		'value',
		'table_reference'
	];

	public function docs_ints()
	{
		return $this->hasMany(DocsInt::class, 'id_value');
	}

	public function docs_strings()
	{
		return $this->hasMany(DocsString::class, 'id_value');
	}
}
