<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Idioma
 * 
 * @property int $id
 * @property string $descripcion
 * @property string $codigo
 * 
 * @property Collection|DocsString[] $docs_strings
 *
 * @package App\Models
 */
class Idioma extends Model
{
	protected $table = 'idioma';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int'
	];

	protected $fillable = [
		'descripcion',
		'codigo'
	];

	public function docs_strings()
	{
		return $this->hasMany(DocsString::class, 'id_idioma');
	}
}
