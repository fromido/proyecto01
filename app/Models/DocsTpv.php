<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DocsTpv
 *
 * @property int $id
 * @property string $name_doc
 * @property Carbon $create
 * @property Carbon $update
 * @property int $tipo_doc
 * @property string $plantilla
 * @property int $ayuntamiento_id
 * @property string $icon
 * @property string $email
 *
 * @property Doc $doc
 * @property Collection|DocsInt[] $docs_ints
 * @property Collection|DocsString[] $docs_strings
 *
 * @package App\Models
 */
class DocsTpv extends Model
{
	protected $table = 'docs_tpv';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'tipo_doc' => 'int',
		'ayuntamiento_id' => 'int'
	];

	protected $dates = [
		'create',
		'update'
	];

	protected $fillable = [
		'name_doc',
		'create',
		'update',
		'tipo_doc',
		'plantilla',
		'ayuntamiento_id',
		'icon',
		'email'
	];

	public function doc()
	{
		return $this->belongsTo(Doc::class, 'tipo_doc');
	}

	public function docs_ints()
	{
		return $this->hasMany(DocsInt::class, 'id_doc');
	}

	public function docs_strings()
	{
		return $this->hasMany(DocsString::class, 'id_doc');
	}


    public function getData($docs,$doc_params){
        foreach ($docs as $d) {
            if(array_key_exists($d->docs_param->value,$doc_params)){
                if(gettype($doc_params[$d->docs_param->value]) == 'array'){
                    array_push($doc_params[$d->docs_param->value],$d->value);
                }else{
                    $doc_params[$d->docs_param->value] = array($doc_params[$d->docs_param->value],$d->value);
                }
            }else{
                $doc_params[$d->docs_param->value]= $d->value;
            }
        }
        return $doc_params;
    }
}
