<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DocsString
 * 
 * @property int $id
 * @property string $value
 * @property int $id_value
 * @property int $id_doc
 * @property int $id_idioma
 * 
 * @property DocsTpv $docs_tpv
 * @property Idioma $idioma
 * @property DocsParam $docs_param
 *
 * @package App\Models
 */
class DocsString extends Model
{
	protected $table = 'docs_string';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'id_value' => 'int',
		'id_doc' => 'int',
		'id_idioma' => 'int'
	];

	protected $fillable = [
		'value',
		'id_value',
		'id_doc',
		'id_idioma'
	];

	public function docs_tpv()
	{
		return $this->belongsTo(DocsTpv::class, 'id_doc');
	}

	public function idioma()
	{
		return $this->belongsTo(Idioma::class, 'id_idioma');
	}

	public function docs_param()
	{
		return $this->belongsTo(DocsParam::class, 'id_value');
	}
}
