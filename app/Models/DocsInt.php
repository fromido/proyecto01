<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DocsInt
 * 
 * @property int $id
 * @property int $value
 * @property int $id_value
 * @property int $id_doc
 * 
 * @property DocsTpv $docs_tpv
 * @property DocsParam $docs_param
 *
 * @package App\Models
 */
class DocsInt extends Model
{
	protected $table = 'docs_int';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'value' => 'int',
		'id_value' => 'int',
		'id_doc' => 'int'
	];

	protected $fillable = [
		'value',
		'id_value',
		'id_doc'
	];

	public function docs_tpv()
	{
		return $this->belongsTo(DocsTpv::class, 'id_doc');
	}

	public function docs_param()
	{
		return $this->belongsTo(DocsParam::class, 'id_value');
	}
}
