<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Entidade
 * 
 * @property int $id
 * @property string $name
 * @property string $url
 *
 * @package App\Models
 */
class Entidade extends Model
{
	protected $table = 'entidades';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int'
	];

	protected $fillable = [
		'name',
		'url'
	];
}
