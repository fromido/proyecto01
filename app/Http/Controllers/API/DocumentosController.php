<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\DocsTpv;
use App\Models\Entidade;
use App\Models\Idioma;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;


class DocumentosController extends Controller {
    /**
     * Handle the incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index($id) {
        //tenemos el acceso por esta funcion ( podria ser
        //nuestra API
        //obtenemos documentos del ayuntamiento
        $docs = DocsTpv::where('ayuntamiento_id', $id)->get();
        //esto es secundaroio obtener el idioma
        $idioma = null;
        if(!\Session::get('idioma')){
            $idioma = 1;
        }else{
            $idioma  = \Session::get('idioma');
        }
        $idiomas = Idioma::select()->get();
        // me interesa solo el id del idioma , aqui fuerzo a que sea 1
        //SI CAMBIO EL IDIOMA
        //la calve es la misma  pero el value es otro

        foreach ($docs as $doc) {
            //miro los ints parametros
            $docs_int = $doc->docs_ints;
            //miro los parametros de string con el idioma que me interese mas
            //los string que no tengan idioma de ese documento
            //EL NULL ESte es xk algunos parametros no tiene  traduccion pero hacen referencia a un parametro de documento
            $docs_string = $doc->docs_strings->whereIn('id_idioma', [$idioma,null]);

            $doc_params = [];
            //montamos el array con los datos de los string y los int
            $doc_params = $doc->getData($docs_int, $doc_params);
            $doc_params = $doc->getData($docs_string, $doc_params);


            $doc->params = $doc_params;
            //esto es mejorable , pero no se me ocurria otra cosa asi rapidamente
            unset($doc->docs_ints);
            unset($doc->docs_strings);
        }
        //DEMOSTRACION
        //return $docs;
        return view('welcome', ['documentos' => $docs,'idiomas'=>$idiomas]);
    }
    public function cambiarIdioma(Request $request) {
        dd("hola");
        $data = $request->all();
        if(isset($data['idioma'])){
            session('idioma', $data['idioma']);
            DocumentosController::index(51);
        }
    }
}
